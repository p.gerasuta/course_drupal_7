<?php


function form_task_form($form, &$form_state) {

  /*$form['#attached']['css'][] = drupal_get_path('module', 'my_form_task') . '/task_form.css';*/

  drupal_add_css(drupal_get_path('module', 'my_form_task') . '/task_form.css');

  drupal_add_js(drupal_get_path('module', 'my_form_task') . '/task_form.js');

  $form['name'] = [
    '#type' => 'textfield',
    '#title' => t('Name'),
    '#description' => t('Your name.'),
    '#required' => TRUE,
  ];


  $form['email'] = [
    '#type' => 'textfield',
    '#title' => t('E-mail'),
    '#description' => t('Your email.'),
    '#required' => TRUE,
  ];

  $year_birth = range(1900,2010);

  $form['year_birth'] = [
    '#type' => 'select',
    '#title' => t('Birth year'),
    '#options' => $year_birth,
  ];


  $form['gender'] = [
    '#type' => 'radios',
    '#title' => t('Your gender'),
    '#options' => [
      '0' => t('Man'),
      '1' => t('Female'),
      '2' => t('Other'),
    ],
    '#required' => TRUE,
    '#default_value' => 0,
  ];


  $form['limbs'] = [
    '#type' => 'radios',
    '#title' => t('Number limbs'),
    '#options' => [
      '0' => ('0'),
      '1' => ('1'),
      '2' => ('3'),
      '3' => ('4'),
      '4' => ('5'),
      '5' => t('More'),
    ],
    '#required' => TRUE,
    '#default_value' => 0,
  ];


  $form['super_power'] = [
    '#type' => 'select',
    '#title' => t('Superpower'),
    '#options' => [
      '0' => t('Immortality'),
      '1' => t('Passing through walls'),
      '2' => t('Levitation'),
    ],
    '#multiple' => TRUE,
    '#required' => TRUE,
  ];


  $form['biography'] = [
    '#type' => 'textarea',
    '#title' => t('Biography'),
    '#required' => TRUE,
  ];

  $form['check'] = [
    '#title' => t('I have read the contract'),
    '#type' => 'checkbox',
    '#required' => TRUE,
  ];


  $form['actions'] = [
    '#type' => 'actions',
  ];

  $form['actions']['submit'] = [
    '#type' => 'submit',
    '#value' => t('Send'),
  ];


  return $form;

}


function form_task_form_submit($form, &$form_state) {

  $super_option = array();
  foreach ($form_state['values']['super_power'] as $key => $super) {
    $super_option [] = $form['super_power']['#options'][$super];
  }

  drupal_set_message(t('Thank you for form'));
  $module = 'my_form_task';
  $key = 'form_task';
  $to = variable_get('site_mail', '');
  global $language;

  $params = array(
    'name' => $form_state['values']['name'],
    'email' => $form_state['values']['email'],
    'year_birth' => $form['year_birth']['#options'][$form_state['values']['year_birth']],
    'gender' => $form['gender']['#options'][$form_state['values']['gender']],
    'limbs' => $form_state['values']['limbs'],
    'super_power' => implode(', ', $super_option),
    'biography' => $form_state['values']['biography']
  );

  drupal_mail($module, $key, $to, $language, $params, $from = NULL, $send = TRUE);


  watchdog('my_form_task', 'Task form  = %params' , array('%params' => implode(',', $params)));

}