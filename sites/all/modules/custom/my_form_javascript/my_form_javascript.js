(function ($, Drupal) {
    Drupal.behaviors.pizzaBehaviour = {
        attach: function (context, settings) {
            var pizzaArray = {};
            var pizza = 0;
            var dist = 0;
            $('.form-select', context).change(function () {
                //console.log($(this).children('option:selected').val());
                pizzaArray[$(this).attr('id')] = $(this).children('option:selected').val();
                /*console.log($(this).attr('id') + " => " + pizzaArray[$(this).attr('id')]);*/
                pizza = 0;
                for (var key in pizzaArray) {
                    pizza += pizzaArray[key] * key;
                }
                $("#price-order").val(Number(Number(pizza) + Number(dist)));
            });

            $('.form-radio', context).change(function () {
                dist = $(this).attr('value');
                console.log(dist);
                $("#price-order").val(Number(Number(pizza) + Number(dist)));
            });


        }
    }
})(jQuery, Drupal);
