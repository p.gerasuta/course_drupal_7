<?php


function form_javascript($form, &$form_state) {


  $query_pizza = db_select('form_javascript_pizza')
    ->fields('form_javascript_pizza', array(
      'name',
      'price',
      'count',
      'available'
    ));

  $result_pizza = $query_pizza->execute();

  $form['pizza'] = [
    '#type' => 'fieldset',
    '#title' => t('Pizza'),
    '#tree' => TRUE,
  ];


  while ($pizza = $result_pizza->fetchAssoc()) {

    if ($pizza['available'] == 1) {


      $form['pizza'][$pizza['name']] = [
        '#type' => 'fieldset',
        '#title' => t($pizza['name'] . ' Price: ' . $pizza['price'] . ' ₽'),
        '#description' => t('Name pizza.'),
        '#collapsible' => TRUE,
        '#collapsed' => TRUE,
        '#tree' => TRUE,
      ];
      $form['pizza'][$pizza['name']]['count'] = array(
        '#type' => 'select',
        '#title' => t('Count of pizza'),
        '#options' => range(0, $pizza['count']),
        '#id' => $pizza['price'],
      );
    }
  }


  $query_blocks = db_select('form_javascript_blocks')
    ->fields('form_javascript_blocks', array('name', 'price'));


  $result_blocks = $query_blocks->execute();

  $blocks_array = array();

  while ($blocks = $result_blocks->fetchAssoc()) {
    $blocks_array[$blocks['price']] = $blocks['name'];
  }


  /*print_r($blocks_array);*/
  $form['district'] = [
    '#title' => t('District'),
    '#type' => 'radios',
    '#options' => $blocks_array,
    '#required' => TRUE
  ];


  $form['phone'] = [
    '#type' => 'textfield',
    '#title' => t('Phone number'),
    '#required' => TRUE
  ];


  $form['address'] = [
    '#type' => 'textfield',
    '#title' => t('Address to order'),
    '#required' => TRUE
  ];


  $form['price-order'] = [
    '#type' => 'textfield',
    '#title' => t('Price order'),
    '#disabled' => TRUE,
    '#id' => 'price-order',
  ];

  $form['submit'] = [
    '#type' => 'submit',
    '#value' => t('To order'),
  ];


  $form['#attached']['js'][] = array(
    'data' => drupal_get_path('module', 'my_form_javascript') . '/my_form_javascript.js',
    'type' => 'file',
  );

  return $form;
}


function form_javascript_validate($form, &$form_state) {

  if (isset($form_state['values']['phone'])) {
    $phone = $form_state['values']['phone'];

    if (!preg_match('^((8|\+7)[\- ]?)?(\(?\d{3}\)?[\- ]?)?[\d\- ]{7,10}$^', $phone)) {
      form_set_error('phone', t('Incorrect form number.'));
    }
  }


}


function form_javascript_submit($form, &$form_state) {
  add_order_in_bd($form_state);

  dpm($form_state);

  $order_price = 0;

  $count_order = ' ';


  foreach ($form_state['values']['pizza'] as $key => $pizza) {

    $count_order .= $key. ' = ' . $pizza['count']. ', ';

    $price = db_select('form_javascript_pizza')
      ->fields('form_javascript_pizza', array('price'))
      ->condition('name', $key, '=')
      ->execute()
      ->fetchField();


    $order_price += $pizza['count'] * $price;
  }

  $order_price += $form_state['values']['district'];



  $module = 'my_form_javascript';
  $key = 'form_javascript';
  $to = variable_get('site_mail', '');
  global $language;

  $params = array(
    'order_price' => $order_price,
    'count_order' => $count_order,
    'phone' => $form_state['values']['phone'],
    'block' => $form['district']['#options'][$form_state['values']['district']],
    'address' => $form_state['values']['address'],
  );

  drupal_mail($module, $key, $to, $language, $params, $from = NULL, $send = TRUE);


  drupal_set_message($order_price);

}


function form_javascript_settings($form, &$form_state) {


 /* $form['#attached']['js'][] = array(
    'data' => drupal_get_path('module', 'my_form_javascript') . '/my_form_javascript_setting.js',
    'type' => 'file',
  );*/

  $form['title_one'] = [
    '#markup' => 'Pizza config',
  ];

  $query_pizza = db_select('form_javascript_pizza')
    ->fields('form_javascript_pizza', array(
      'name',
      'price',
      'count',
      'available'
    ));

  $result_pizza = $query_pizza->execute();
  while ($pizza = $result_pizza->fetchAssoc()) {

    if ($pizza['available'] == 1) {
      $available = TRUE;
    }
    else {
      $available = FALSE;
    }
    $form['pizza_' . $pizza['name']] = [
      '#type' => 'fieldset',
      '#title' => t($pizza['name']),
      '#description' => t('Name pizza.'),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
      '#tree' => TRUE,
    ];
    $form['pizza_' . $pizza['name']]['count'] = [
      '#markup' => $pizza['count'],
    ];
    $form['pizza_' . $pizza['name']]['count_change'] = [
      '#type' => 'textfield',
      '#title' => t('Change count of pizza'),
    ];

    $form['pizza_' . $pizza['name']]['price'] = [
      '#markup' => $pizza['price'] . ' ₽',
    ];


    $form['pizza_' . $pizza['name']]['price_change'] = [
      '#type' => 'textfield',
      '#title' => t('Change price of pizza'),
    ];

    if ($pizza['count'] == 0) {
      $disabled = TRUE;
    }
    else {
      $disabled = FALSE;
    }

    $form['pizza_' . $pizza['name']]['available'] = [
      '#type' => 'checkbox',
      '#title' => t('Available'),
      '#default_value' => $available,
      '#disabled' => $disabled,
    ];
  }

  $form['title_two'] = [
    '#markup' => 'Blocks config',
  ];

  $query_blocks = db_select('form_javascript_blocks')
    ->fields('form_javascript_blocks', array(
      'name',
      'price',
    ));


  $result_blocks = $query_blocks->execute();
  while ($blocks = $result_blocks->fetchAssoc()) {
    $form['block_' . $blocks['name']] = [
      '#type' => 'fieldset',
      '#title' => t($blocks['name']),
      '#description' => t('Name Block.'),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
      '#tree' => TRUE,
    ];

    $form['block_' . $blocks['name']]['price'] = [
      '#markup' => $blocks['price'] . ' ₽',
    ];


    $form['block_' . $blocks['name']]['price_change'] = [
      '#type' => 'textfield',
      '#title' => t('Change price of pizza'),
    ];
  }

  $form['submit'] = [
    "#type" => "submit",
    '#value' => t('Change config'),
  ];


  return $form;

}

function form_javascript_settings_submit($form, &$form_state) {
  foreach ($form_state['complete form'] as $key => $pizza) {

    if (strpos($key, 'pizza') !== FALSE) {

      if ($form_state['values'][$key]['count_change']) {
        change_pizza_bd($key, $form_state['values'][$key]['count_change'], 'form_javascript_pizza', 'count', 'pizza_');
      }

      if ($form_state['values'][$key]['price_change']) {
        change_pizza_bd($key, $form_state['values'][$key]['price_change'], 'form_javascript_pizza', 'price', 'pizza_');
      }

      if ($form_state['values'][$key]['available'] !== FALSE) {
        change_pizza_bd($key, $form_state['values'][$key]['available'], 'form_javascript_pizza', 'available', 'pizza_');
      }

    }
  }

  foreach ($form_state['complete form'] as $key => $blocks) {


    if (strpos($key, 'block') !== FALSE) {
      drupal_set_message($key);
      if ($form_state['values'][$key]['price_change']) {
        change_pizza_bd($key, $form_state['values'][$key]['price_change'], 'form_javascript_blocks', 'price', 'block_');
      }
    }
  }

}


function change_pizza_bd($name, $value, $table, $field, $clear_str) {
  $name = str_replace($clear_str, '', $name);
  db_update($table)
    ->condition('name', $name)
    ->fields(array(
      $field => $value,
    ))
    ->execute();
}


function add_order_in_bd($form_state) {


  foreach ($form_state['values']['pizza'] as $key => $pizza) {


    if ($pizza['count'] !== 0) {

      $count = db_select('form_javascript_pizza')
        ->fields('form_javascript_pizza', array('count'))
        ->condition('name', $key, '=')
        ->execute()
        ->fetchField();
      $count -= $pizza['count'];

      db_update('form_javascript_pizza')
        ->condition('name', $key)
        ->fields(array(
          'count' => $count,
        ))
        ->execute();

      if ($count == 0) {
        db_update('form_javascript_pizza')
          ->condition('name', $key)
          ->fields(array(
            'available' => 0,
          ))
          ->execute();
      }

    }

  }


}